FROM debian:jessie-slim
MAINTAINER lukman - lukman@bukalapak.com

RUN set -ex; \
    \
    Deps=' \
      autoconf \
      build-essential \
      dh-autoreconf \
      git \
      libssl-dev \
      libtool \
      python-software-properties \
      tcl8.5 \
      wget \
      ca-certificates \
    '; \
    apt-get update; \
    apt-get install -y $Deps --no-install-recommends; \
    rm -rf /var/lib/apt/lists/*

RUN git clone https://github.com/Netflix/dynomite.git
#COPY startup.sh dynomite/startup.sh

WORKDIR dynomite/
RUN git checkout v0.5.9
RUN autoreconf -fvi \
    && ./configure --enable-debug=log \
    && CFLAGS="-ggdb3 -O0" ./configure --enable-debug=full \
    && make \
    && make install

EXPOSE 8101 22122 22222 8102
#RUN sysctl vm.overcommit_memory=1

#ENTRYPOINT ["/dynomite/startup.sh"]
